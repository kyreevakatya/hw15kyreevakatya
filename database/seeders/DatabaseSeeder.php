<?php

namespace Database\Seeders;

use App\Models\Ad;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Models\User::factory()->count(10)->create();

        $ads = Ad::factory()->count(10)->make(['user_id' => null]);

        $ads->each(function (Ad $ads) use ($users) {
            $ads->user()->associate($users->random());
            $ads->save();
        });
    }
}
