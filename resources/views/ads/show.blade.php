@extends('layout')

@section('title', 'Ad')

@section('body')
    <a href="{{route('home')}}" type="button" class="btn btn-outline-primary">Back</a>
    <h4><strong>{{ $ad->title }}</strong></h4>
        <td>
            @can('update', $ad)
                <a href="{{route('ads.create', $ad->id)}}">Edit</a>|
            @endcan
            @can('delete', $ad)
                <a href="{{route('ads.delete', $ad->id)}}">Delete</a>
            @endcan
        </td>
    <h5>{{$ad->user->username}}</h5>
    <p>{{ $ad->created_at->diffForHumans()}}</p>
    <p><em>{{ $ad->description }}</em></p>
@endsection
