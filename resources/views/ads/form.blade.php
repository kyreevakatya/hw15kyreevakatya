@extends('layout')

@section('title', 'form')

@section('body')
    <a href="{{route('home')}}" type="button" class="btn btn-outline-primary">Back</a>
    <form method="post">
        <div class="mb-3">
            <label for="title" class="form-label">Title</label>
            <input type="text" name="title"
                   class="form-control @if($errors->has('title')) is-invalid @endif" id="title"
                   value="{{old('title',$ads->title ?? '')}}">
            <div class="invalid-feedback">
                <p>{{$errors->first('title')}}</p>
            </div>
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <textarea class="form-control @if($errors->has('description')) is-invalid @endif" name="description"
                      id="description" rows="3">
                  {{old('description',$ads->description ?? '')}}
                  </textarea>
            <div class="invalid-feedback">
                <p>{{$errors->first('description')}}</p>
            </div>
        </div>
        <div class="mb-3">
            <input type="submit" class="btn btn-primary mb-3" value="@if(isset($ads->id)) Save @else Create @endif"/>
        </div>
        @csrf
    </form>
@endsection
