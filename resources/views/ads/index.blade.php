@extends('layout')

@section('title', 'Homepage')

@section('body')
    @auth
        <a href="{{route('ads.create')}}" type="button" class="btn btn-outline-primary">Create Ad</a>
    @endauth
    @foreach($ads as $ad)
        <h4><strong><a href="{{ route('ads.show', $ad->id) }}">{{ $ad->title }}</a></strong></h4>
        <td>
            @can('update', $ad)
                <a href="{{route('ads.create', $ad->id)}}">Edit</a>|
            @endcan
            @can('delete', $ad)
                <a href="{{route('ads.delete', $ad->id)}}">Delete</a>
            @endcan
        </td>
        <h5>{{$ad->user->username}}</h5>
        <p>{{ $ad->created_at->diffForHumans()}}</p>
        <p><em>{{ $ad->description }}</em></p>
    @endforeach

    {{$ads->links()}}
@endsection
