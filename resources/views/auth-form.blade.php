<form method="post" action="/login">
    @csrf
    <div class="mb-3">
        <label for="username" class="form-label">Username</label>
        <input type="text" class="form-control" name="username"
               id="username" value="{{old('username')}}">
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">Password</label>
        <input type="password" class="form-control @if($errors->has('password')) is-invalid @endif " name="password"
               id="exampleInputPassword1" value="{{old('password')}}">
        <div class="invalid-feedback">
            <p>{{$errors->first('password')}}</p>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Login</button>
</form>

<p><a href="{{$oauth_dropbox_uri}}">Login using Dropbox</a></p>
