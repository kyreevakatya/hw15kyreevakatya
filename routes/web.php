<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [\App\Http\Controllers\AdController::class, 'index'])->name('home');
Route::get('/{id}', [\App\Http\Controllers\AdController::class, 'show'])->whereNumber('id')->name('ads.show');

Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login'])->middleware('guest');
Route::get('/logout', [\App\Http\Controllers\AuthController::class, 'logout'])->name('logout')->middleware('auth');

Route::get('/edit/{ads?}', [\App\Http\Controllers\AdController::class, 'create'])
    ->name('ads.create')->middleware('auth');
Route::post('/edit/{ads?}', [\App\Http\Controllers\AdController::class, 'save'])->middleware('auth');

Route::get('/delete/{ads}', [\App\Http\Controllers\AdController::class, 'delete'])
    ->middleware('can:delete,ads')->name('ads.delete');

Route::get('/callback', [\App\Http\Controllers\OAuthController::class, 'callback']);
