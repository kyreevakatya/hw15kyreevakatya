#Тестовое задание: сайт объявлений

Чтобы развернуть тестовое задание используйте команды:

docker run --rm --interactive --tty --volume $(pwd):/app composer install

docker-compose up -d

php artisan migrate --seed
