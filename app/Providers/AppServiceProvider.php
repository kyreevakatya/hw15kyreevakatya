<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Nette\Utils\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Illuminate\Pagination\Paginator::useBootstrap();

        $parameters = [
            'client_id' => config('oauth.dropbox.client_id'),
            'redirect_uri' => config('oauth.dropbox.redirect_uri'),
            'response_type' => 'code',
        ];

        View::share('oauth_dropbox_uri', 'https://www.dropbox.com/oauth2/authorize?' . http_build_query($parameters));
    }
}
