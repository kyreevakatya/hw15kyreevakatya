<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\User;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdController extends Controller
{
    public function index()
    {
        $ads = Ad::orderBy('id', 'desc')->paginate(5);

        return view('ads.index', compact('ads'));
    }

    public function show($id)
    {
        $ad = Ad::find($id);

        return view('ads.show', compact('ad'));
    }

    public function create(Ad $ads = null)
    {
        if (isset($ads) && !Auth::user()->can('update', $ads)) {
            return abort(403, 'Unauthorized action.');
        }

        return view('ads.form', compact('ads'));
    }

    public function save(Request $request, Ad $ads = null)
    {

        $data = $request->validate([
            'title' => ['required'],
            'description' => ['required']
        ]);
        $data['user_id'] = Auth::id();

        $ads = Ad::updateOrCreate(['id' => $ads->id ?? null], $data);

        return redirect()->route('ads.show', $ads->id);
    }

    public function delete(Ad $ads)
    {

        $ads->delete();

        return redirect()->route('home');
    }
}
