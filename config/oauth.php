<?php

return [
    'dropbox' => [
        'client_id' => env('OAUTH_DROPBOX_CLIENT_ID'),
        'secret_key' => env('OAUTH_DROPBOX_SECRET_KEY'),
        'redirect_uri' => env('OAUTH_DROPBOX_REDIRECT_URI'),
    ]
];
